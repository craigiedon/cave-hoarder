package com.craig.caveHoarder;

import java.util.Iterator;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class CaveHoarder extends Game{
	SpriteBatch batch;
	ShapeRenderer shapeDrawer;
	BitmapFont font;
	BitmapFont fontLarge;
	BitmapFont impactFont;
	Skin skin;
	Music caveAmbience;
	
	public void create(){
		batch = new SpriteBatch();
		//Use libGDX's default arial font
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/MarsAttacks.ttf"));
		FreeTypeFontGenerator generatorImpact = new FreeTypeFontGenerator(Gdx.files.internal("fonts/IMPACT.TTF"));
		font = generator.generateFont(72);
		fontLarge = generator.generateFont(100);
		impactFont = generatorImpact.generateFont(28);
		Texture panelBG = new Texture(Gdx.files.internal("panelBG.png"));
		
		shapeDrawer = new ShapeRenderer();
		skin = new Skin();
		skin.add("marsAttacksLarge", fontLarge);
		skin.add("marsAttacksMedium", font);
		skin.add("impact", impactFont);
		skin.add("panelBG", panelBG);
		skin.addRegions(new TextureAtlas(Gdx.files.internal("data/uiskin.atlas")));
		skin.addRegions(new TextureAtlas(Gdx.files.internal("caveButton.atlas")));
		skin.load(Gdx.files.internal("data/uiskin.json"));
		
		caveAmbience = Gdx.audio.newMusic(Gdx.files.internal("sound/EmptyColonies.mp3"));
		caveAmbience.play();
		caveAmbience.setVolume(0.3f);
		
//		this.setScreen(new GameScreen(this, "level1.json"));
		this.setScreen(new MenuScreen(this));
//		this.setScreen(new LevelEditor(this));
//		this.setScreen(new LevelSelect(this));
//		this.setScreen(new WinScreen(this, 1, "level1.json"));
	}
	
	public void dispose(){
		batch.dispose();
		caveAmbience.stop();
		caveAmbience.dispose();
	}
	
	public void render(){
		super.render();
	}
	
}