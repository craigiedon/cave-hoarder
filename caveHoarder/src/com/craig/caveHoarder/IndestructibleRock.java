package com.craig.caveHoarder;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class IndestructibleRock extends Cell{
	public boolean isDestructible(){
		return false;
	}

	@Override
	public void destroyCell(){
		System.out.println("You shouldnt be destroying this thing!");
	}
	
	public IndestructibleRock(float x, float y , float sideLength){
		super(x,y,sideLength);
	}

	@Override
	public void render(SpriteBatch batch){
		batch.draw(GameAssets.rock, x, y);
		if(hasGem()){
			batch.draw(GameAssets.gem, gem.x, gem.y);
		}
	}
}
