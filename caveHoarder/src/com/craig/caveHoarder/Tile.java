package com.craig.caveHoarder;

import com.badlogic.gdx.math.Rectangle;

public class Tile extends Rectangle {
	public TileType tileType;
	
	public Tile(int x, int y, int size){
		width = size;
		height = size;
		this.x = x;
		this.y = y;
		
		tileType = TileType.DIRT;
	}
	
	public Tile(){
		super();
	}
	
	public void printDetails(){
		System.out.println(width);
		System.out.println(height);
		System.out.println(x);
		System.out.println(y);

		
	}

}
