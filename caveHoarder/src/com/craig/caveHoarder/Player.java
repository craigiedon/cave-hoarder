package com.craig.caveHoarder;

import java.util.Arrays;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.Gdx;

public class Player extends InputAdapter {

	Goblin goblin;
	Troll troll;
	Grid gameGrid;
	PlayerTurnState state;
	int[] selectedBlock;
	public int turnsLeft = 3;
	public int playerNumber;
	public int gemsCollected = 0;
	Sound trollStartGrunt;
	Sound goblinStartGrunt;
	Sound switchSound;
	Sound destroyBlockSound;
	Sound createBlockSound;
	Sound gemGet;
	
	public Player(int pNum, Grid grid, PlayerTurnState startingState, int goblinRow, int goblinCol, int trollRow, int trollCol){
		gameGrid = grid;		
		
		playerNumber = pNum;
		goblin = new Goblin(goblinRow, goblinCol, gameGrid, pNum);
		troll = new Troll(trollRow , trollCol, gameGrid, pNum);
		
		gameGrid.getCell(goblin.getRow(), goblin.getCol()).destroyCell();
		gameGrid.getCell(troll.getRow(), troll.getCol()).destroyCell();
		
		state = startingState;
		selectedBlock = null;
		trollStartGrunt = Gdx.audio.newSound(Gdx.files.internal("sound/troll.mp3"));
		goblinStartGrunt = Gdx.audio.newSound(Gdx.files.internal("sound/goblin.mp3"));
		switchSound = Gdx.audio.newSound(Gdx.files.internal("sound/switchWeapon.wav"));
		destroyBlockSound = Gdx.audio.newSound(Gdx.files.internal("sound/destroyBlock.wav"));
		createBlockSound = Gdx.audio.newSound(Gdx.files.internal("sound/createBlock.wav"));
		gemGet = Gdx.audio.newSound(Gdx.files.internal("sound/gemGet.wav"));
	}
	
	public void startTurn(){
		goblinStartGrunt.play();
		turnsLeft = 3;
		state = PlayerTurnState.MOVE_GOBLIN;
	}
	
	public void render(SpriteBatch batch){
		
		//Draw goblin
		goblin.render(batch);
        
        //Draw troll
		troll.render(batch);

		//Draw selection boxes
		if(state == PlayerTurnState.USE_TOOL){
			if(goblin.currentTool == Tool.DESTROY){
				for(int i = -1; i <= 1; i++){
					for(int j = -1; j <= 1; j++){
						if(!(i == 0 && j == 0) &&
								(i != j) &&
								(i != -j) &&
								goblin.getRow() + i < gameGrid.numRows() &&
								goblin.getRow() + i >= 0 &&
								goblin.getCol() + j < gameGrid.numCols() &&
								goblin.getCol() + j >= 0){
							Cell highlightedSquare = gameGrid.getCell(goblin.getRow() + i, goblin.getCol() + j);
							if(highlightedSquare.isDestructible()){
								batch.draw(GameAssets.destroyHighlightTex, highlightedSquare.x, highlightedSquare.y);
							}
						}

					}
				}
				if(selectedBlock != null){
					Cell selectedSquare = gameGrid.getCell(selectedBlock[0], selectedBlock[1]);
						batch.draw(GameAssets.destroySelectTex, selectedSquare.x, selectedSquare.y);
				}
			}
			else if(goblin.currentTool == Tool.CREATE){
				for(int i = -1; i <= 1; i++){
					for(int j = -1; j <= 1; j++){
						if(!(i == 0 && j == 0) &&
								(i != j) &&
								(i != -j) &&
								goblin.getRow() + i < gameGrid.numRows() &&
								goblin.getRow() + i >= 0 &&
								goblin.getCol() + j < gameGrid.numCols() &&
								goblin.getCol() + j >= 0){
							Cell highlightedSquare = gameGrid.getCell(goblin.getRow() + i, goblin.getCol() + j);
							if(highlightedSquare.isDestroyed() && !highlightedSquare.hasGem() && !gameGrid.isOccupied(goblin.getRow() + i, goblin.getCol() + j)){
								batch.draw(GameAssets.createHighlightTex, highlightedSquare.x, highlightedSquare.y);
							}
						}

					}
				}
				if(selectedBlock != null){
					Cell selectedSquare = gameGrid.getCell(selectedBlock[0], selectedBlock[1]);
					batch.draw(GameAssets.createSelectTex, selectedSquare.x, selectedSquare.y);
				}

			}
		}
	}
	
	public void update(){
		goblin.update();
		troll.update();
		
		//Check if you have moved onto a gem
		if(gameGrid.hasGem(troll.getRow(), troll.getCol())){
			gemGet.play();
			gameGrid.removeGem(troll.getRow(), troll.getCol());
			gemsCollected++;
		}
		
	}
	
	public boolean keyDown(int keyCode){
		
		switch(state){
		case MOVE_GOBLIN:
			switch(keyCode){
			case Keys.LEFT:
				goblin.vx = -75;
				break;
			case Keys.RIGHT:
				goblin.vx = 75;
				break;
			case Keys.UP:
				goblin.vy = 75;
				break;
			case Keys.DOWN:
				goblin.vy = -75;
				break;
			case Keys.E:
				switchSound.play();
				if(goblin.currentTool == Tool.DESTROY){
					goblin.currentTool = Tool.CREATE;
				}
				else{
					goblin.currentTool = Tool.DESTROY;
				}
				break;
			case Keys.SPACE:
				//Set selected goblin.getCol() to available choices, if no choices available, dont go into destroy state
				goblin.vx = 0;
				goblin.vy = 0;
				if(goblin.currentTool == Tool.DESTROY && destructibleBlocks()){
						state = PlayerTurnState.USE_TOOL;
				}
				else if(goblin.currentTool == Tool.CREATE && creatableBlocks()){
						state = PlayerTurnState.USE_TOOL;
				}
				break;
			case Keys.ENTER:
				goblin.vx = 0;
				goblin.vy = 0;
				changeToTroll();
				break;
			}
			
			break;
		case USE_TOOL:
			if(keyCode == Keys.ESCAPE || keyCode == Keys.C){
				state = PlayerTurnState.MOVE_GOBLIN;
			}
			else if(goblin.currentTool == Tool.DESTROY){
				handleDestroyControls(keyCode);
			}
			else if(goblin.currentTool == Tool.CREATE){
				handleCreateControls(keyCode);
			}
			break;
		case MOVE_TROLL:

			switch(keyCode){
			case Keys.LEFT:
				troll.ax = -10;
				troll.facing = Directions.WEST;
				if(Gdx.input.isKeyPressed(Keys.RIGHT)){
					troll.vx = 0;
				}
				break;
			case Keys.RIGHT:
				troll.ax = 10;
				troll.facing = Directions.EAST;
				if(Gdx.input.isKeyPressed(Keys.LEFT)){
					troll.vx = 0;
				}
				break;
			case Keys.UP:
				troll.jump();
				break;
			case Keys.SPACE:
				troll.jump();
				break;
			case Keys.ENTER:
				troll.ax = 0;
				troll.ay = 0;
				state = PlayerTurnState.FINISHED;
			}						
		default:
			return false;
		}

		
		return true;
	}
	
	public boolean keyUp(int keyCode){
		switch(state){
		case MOVE_GOBLIN:			

			switch(keyCode){
			case Keys.LEFT:
				if(Gdx.input.isKeyPressed(Keys.RIGHT)){
					goblin.vx = 75;
				}
				else{
					goblin.vx = 0;
				}
				break;
			case Keys.RIGHT:
				if(Gdx.input.isKeyPressed(Keys.LEFT)){
					goblin.vx = -75;
				}
				else{
					goblin.vx = 0;
				}
				break;
			case Keys.UP:
				if(Gdx.input.isKeyPressed(Keys.DOWN)){
					goblin.vy = -75;
				}
				else{
					goblin.vy = 0;
				}
				break;
			case Keys.DOWN:
				if(Gdx.input.isKeyPressed(Keys.UP)){
					goblin.vy = 75;
				}
				else{
					goblin.vy = 0;
				}
				break;
			}
			return true;
		case USE_TOOL:
			if(selectedBlock != null){
				if((keyCode == Keys.LEFT && Arrays.equals(selectedBlock, goblin.blockLeft())) ||
				   (keyCode == Keys.RIGHT && Arrays.equals(selectedBlock, goblin.blockRight())) ||
				   (keyCode == Keys.UP && Arrays.equals(selectedBlock,  goblin.blockAbove())) ||
				   (keyCode == Keys.DOWN && Arrays.equals(selectedBlock, goblin.blockBelow()))){
					selectedBlock = null;
				}
			}
			return true;
		case MOVE_TROLL:

			switch(keyCode){
			case Keys.LEFT:
				troll.vx = 0;
				if(Gdx.input.isKeyPressed(Keys.RIGHT)){
					troll.facing = Directions.EAST;
					troll.ax = 10;
				}
				else{
					troll.ax = 0;
				}
				break;
			case Keys.RIGHT:
				troll.vx = 0;
				if(Gdx.input.isKeyPressed(Keys.LEFT)){
					troll.facing = Directions.WEST;
					troll.ax = -10;
				}
				else{
					troll.ax = 0;
				}
				break;
			case Keys.UP:
				break;
			}
			return true;
			
		default:
			return false;
		}
	}
	
	private void handleDestroyControls(int keyCode){
		if(keyCode == Keys.SPACE){
			if(selectedBlock != null){
				destroyBlockSound.play();
				gameGrid.getCell(selectedBlock[0],selectedBlock[1]).destroyCell();
				turnsLeft -= 1;
				
				if(turnsLeft== 0){
					changeToTroll();
				}else{
					state = PlayerTurnState.MOVE_GOBLIN;
				}
			}
		}
		if(keyCode == Keys.UP && goblin.blockAbove()[1] < gameGrid.numCols() && gameGrid.getCell(goblin.getRow(), goblin.blockAbove()[1]).isDestructible()){
			selectedBlock = goblin.blockAbove();
		}
		if(keyCode == Keys.DOWN && goblin.blockBelow()[1] >= 0 && gameGrid.getCell(goblin.getRow(), goblin.blockBelow()[1]).isDestructible()){
			selectedBlock = goblin.blockBelow();
		}
		if(keyCode == Keys.RIGHT && goblin.blockRight()[0] < gameGrid.numRows() && gameGrid.getCell(goblin.blockRight()[0], goblin.getCol()).isDestructible()){
			selectedBlock = goblin.blockRight();
		}
		if(keyCode == Keys.LEFT && goblin.blockLeft()[0] >= 0 && gameGrid.getCell(goblin.blockLeft()[0],goblin.getCol()).isDestructible()){
			selectedBlock = goblin.blockLeft();
		}
		if(keyCode == Keys.E){
			switchSound.play();
			if(creatableBlocks()){
				goblin.currentTool = Tool.CREATE;
			}
		}
	}

	private void handleCreateControls(int keyCode){
		if(keyCode == Keys.SPACE){
			if(selectedBlock != null){
				createBlockSound.play();
				gameGrid.getCell(selectedBlock[0],selectedBlock[1]).createCell();
				turnsLeft -= 1;
			}
			if(turnsLeft== 0){
				changeToTroll();
			}
			else{
				state = PlayerTurnState.MOVE_GOBLIN;
			}
		}

		if(keyCode == Keys.UP && goblin.blockAbove()[1] < gameGrid.numCols() && gameGrid.isCreatable(goblin.blockAbove()[0], goblin.blockAbove()[1])){
			selectedBlock = goblin.blockAbove();
		}
		if(keyCode == Keys.DOWN && goblin.blockBelow()[1] >= 0 && gameGrid.isCreatable(goblin.blockBelow()[0], goblin.blockBelow()[1])){
			selectedBlock = goblin.blockBelow();
		}
		if(keyCode == Keys.RIGHT && goblin.blockRight()[0] < gameGrid.numRows() && gameGrid.isCreatable(goblin.blockRight()[0], goblin.blockRight()[1])){
			selectedBlock = goblin.blockRight();
		}
		if(keyCode == Keys.LEFT && goblin.blockLeft()[0] >= 0 && gameGrid.isCreatable(goblin.blockLeft()[0],goblin.blockLeft()[1])){
			selectedBlock = goblin.blockLeft();
		}
		
		if(keyCode == Keys.E && destructibleBlocks()){
			switchSound.play();
			goblin.currentTool = Tool.DESTROY;
		}
	}

	private boolean destructibleBlocks(){
		selectedBlock = null;
		if(goblin.blockAbove()[1] < gameGrid.numCols() && gameGrid.getCell(goblin.getRow(),goblin.blockAbove()[1]).isDestructible()){
			return true;
		}
		else if(goblin.blockBelow()[1] >= 0 && gameGrid.getCell(goblin.getRow(), goblin.blockBelow()[1]).isDestructible()){
			return true;
		}
		else if(goblin.blockRight()[0] < gameGrid.numRows() && gameGrid.getCell(goblin.blockRight()[0], goblin.getCol()).isDestructible()){
			return true;
		}
		else if(goblin.blockLeft()[0] >= 0 && gameGrid.getCell(goblin.blockLeft()[0], goblin.getCol()).isDestructible()){
			return true;
		}
		else{
			return false;
		}
	}
	
	private boolean creatableBlocks(){
		selectedBlock = null;
		if(goblin.blockAbove()[1] < gameGrid.numCols() && gameGrid.isCreatable(goblin.blockAbove()[0], goblin.blockAbove()[1])){
			return true;
		}
		else if(goblin.blockBelow()[1] >= 0 && gameGrid.isCreatable(goblin.blockBelow()[0], goblin.blockBelow()[1])){
			return true;
		}
		else if(goblin.blockRight()[0] < gameGrid.numRows() && gameGrid.isCreatable(goblin.blockRight()[0], goblin.blockRight()[1])){
			return true;
		}
		else if(goblin.blockLeft()[0] >= 0 && gameGrid.isCreatable(goblin.blockLeft()[0], goblin.blockLeft()[1])){
			return true;
		}
		else{
			return false;
		}
	}
	
	public void changeToTroll(){
		trollStartGrunt.play();
		state = PlayerTurnState.MOVE_TROLL;
	}
}
