package com.craig.caveHoarder;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

public class LevelThumbNail extends Button {
	Tile[][] levelData;
	float scale;
	CaveHoarder game;
	String fileName;
	
	public LevelThumbNail(Tile[][] levelDat, CaveHoarder gam, String fName){
		super();
		levelData = levelDat;
		game = gam;
		scale = 100.0f / (GameScreen.CELL_SIZE * levelData.length);
		fileName = fName;
		setWidth(getPrefWidth());
		setHeight(getPrefHeight());
		setTouchable(Touchable.enabled);
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha){
		validate();
		Matrix4 transformMatrix = new Matrix4();
		transformMatrix.translate(getX(), getY(), 0);
		transformMatrix.scale(scale, scale, 1);
		batch.setTransformMatrix(transformMatrix);

		for (Tile[] tileRow : levelData){
			for(Tile tile : tileRow){
				if(tile.tileType == TileType.DIRT){
					batch.draw(GameAssets.dirtBlock, tile.x, tile.y);
				}
				else if(tile.tileType == TileType.EMPTY){
					batch.draw(GameAssets.emptyBlock, tile.x, tile.y);
				}
				else if(tile.tileType == TileType.ROCK){
					batch.draw(GameAssets.rock, tile.x, tile.y);
				}
				else if(tile.tileType == TileType.GEM){
					batch.draw(GameAssets.gem, tile.x, tile.y);
				}
				else if(tile.tileType == TileType.P1_TROLL){
					batch.draw(GameAssets.p1TrollTex, tile.x, tile.y, 0, 0, 25, 25);
				}
				else if(tile.tileType == TileType.P1_GOBLIN){
					batch.draw(GameAssets.p1GoblinTex, tile.x, tile.y, 0,0, 20, 20);
				}
				else if(tile.tileType == TileType.P2_GOBLIN){
					batch.draw(GameAssets.p2GoblinTex, tile.x, tile.y, 0, 0, 20, 20);
				}
				else if(tile.tileType == TileType.P2_TROLL){
					batch.draw(GameAssets.p2TrollTex, tile.x, tile.y, 0, 0, 25, 25);
				}
			}
		}
		
		if(isChecked()){
			batch.end();
			game.shapeDrawer.setTransformMatrix(batch.getTransformMatrix());
			game.shapeDrawer.begin(ShapeType.Line);
			game.shapeDrawer.setColor(Color.GREEN);
			game.shapeDrawer.rect(0, 0, levelData.length * GameScreen.CELL_SIZE, levelData.length * GameScreen.CELL_SIZE);
			game.shapeDrawer.setTransformMatrix(new Matrix4());
			game.shapeDrawer.end();
			batch.begin();
		}
		
		batch.setTransformMatrix(new Matrix4());
		
	}
	
	@Override
	public float getPrefHeight(){
		return levelData.length * GameScreen.CELL_SIZE * scale;
	}
	
	@Override
	public float getPrefWidth(){
		return levelData[0].length * GameScreen.CELL_SIZE * scale;
	}
	
	public float getMaxHeight(){
		return levelData.length * GameScreen.CELL_SIZE * scale;

	}
	
	public float getMaxWidth(){
		return levelData[0].length * GameScreen.CELL_SIZE * scale;
	}
}
