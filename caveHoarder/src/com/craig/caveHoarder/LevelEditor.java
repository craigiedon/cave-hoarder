package com.craig.caveHoarder;

import java.awt.FileDialog;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

public class LevelEditor extends JFrame implements Screen{

	Tile[][] grid;
	Stage stage;
	CaveHoarder game;
	ShapeRenderer sDrawer;
	Vector3 mousePos;
	OrthographicCamera camera;
	Texture filledTex;
	Texture listTex;
	int numRows;
	int numCols;
	SelectBox numRowsSelect;
	SelectBox numColsSelect;
	SelectBox loadList;
	Skin skin;
	
	TileType currentTool;
	
	
	public LevelEditor(CaveHoarder g){
		game = g;
		numRows = 10;
		numCols = 10;
		grid = new Tile[numRows][numCols];
		
        GameAssets.loadTextures();
		currentTool = TileType.DIRT;
    	Pixmap filledPix = new Pixmap(GameScreen.CELL_SIZE, GameScreen.CELL_SIZE, Format.RGBA8888);
    	filledPix.setColor(Color.GREEN);
    	filledPix.fillRectangle(0, 0, GameScreen.CELL_SIZE, GameScreen.CELL_SIZE);
    	filledTex = new Texture(filledPix);
    	filledPix.dispose();
    	
		setupUI();
		sDrawer = game.shapeDrawer;
		for(int row = 0; row < grid.length; row++){
			for(int col = 0; col < grid[row].length; col++){
				grid[row][col] = new Tile(row * GameScreen.CELL_SIZE, col * GameScreen.CELL_SIZE, GameScreen.CELL_SIZE);
			}
		}
		
    	camera = new OrthographicCamera();
    	camera.setToOrtho(false, 800, 480);
    	mousePos = new Vector3(0,0,0);
    	

	}
	
	public void setupUI(){
		skin = game.skin;
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		Table table = new Table();
		table.setFillParent(true);
		
		Label rowsLabel = new Label("Number of Rows", skin);
		Label colsLabel = new Label("Number of Cols", skin);
		
		Object[] dimensionOptions = new Object[]{10,15};
		numRowsSelect = new SelectBox(dimensionOptions, skin);
		numColsSelect = new SelectBox(dimensionOptions, skin);
		
		Label toolLabel = new Label("Tile select", skin);
		
		TextureRegionDrawable dirtDraw = new TextureRegionDrawable(new TextureRegion(GameAssets.dirtBlock));
		TextureRegionDrawable emptyDraw = new TextureRegionDrawable(new TextureRegion(GameAssets.emptyBlock));
		TextureRegionDrawable rockDraw = new TextureRegionDrawable(new TextureRegion(GameAssets.rock));
		TextureRegionDrawable gemDraw = new TextureRegionDrawable (new TextureRegion(GameAssets.gem));
		TextureRegionDrawable p1TrollDraw = new TextureRegionDrawable(new TextureRegion(GameAssets.p1TrollTex, 0,0,25,25));
		TextureRegionDrawable p1GoblinDraw = new TextureRegionDrawable(new TextureRegion(GameAssets.p1GoblinTex,0,0,20,20));
		TextureRegionDrawable p2TrollDraw = new TextureRegionDrawable(new TextureRegion(GameAssets.p2TrollTex,0,0,25,25));
		TextureRegionDrawable p2GoblinDraw = new TextureRegionDrawable(new TextureRegion(GameAssets.p2GoblinTex,0,0,20,20));
		
		ImageButton dirtButton = new ImageButton(dirtDraw);
		dirtButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				currentTool = TileType.DIRT;
				System.out.print("Tool is now: " + currentTool);
			}
		});
		ImageButton emptyButton = new ImageButton(emptyDraw);
		emptyButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				currentTool = TileType.EMPTY;
				System.out.print("Tool is now: " + currentTool);
			}
		});
		ImageButton rockButton = new ImageButton(rockDraw);
		rockButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				currentTool = TileType.ROCK;
				System.out.print("Tool is now: " + currentTool);
			}
		});
		ImageButton gemButton = new ImageButton(gemDraw);
		gemButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				currentTool = TileType.GEM;
				System.out.print("Tool is now: " + currentTool);
			}
		});
		ImageButton p1TrollButton = new ImageButton(p1TrollDraw);
		p1TrollButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				currentTool = TileType.P1_TROLL;
				System.out.print("Tool is now: " + currentTool);
			}
		});
		ImageButton p1GoblinButton = new ImageButton(p1GoblinDraw);
		p1GoblinButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				currentTool = TileType.P1_GOBLIN;
				System.out.print("Tool is now: " + currentTool);
			}
		});
		ImageButton p2TrollButton = new ImageButton(p2TrollDraw);
		p2TrollButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				currentTool = TileType.P2_TROLL;
				System.out.print("Tool is now: " + currentTool);
			}
		});
		ImageButton p2GoblinButton = new ImageButton(p2GoblinDraw);
		p2GoblinButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				currentTool = TileType.P2_GOBLIN;
				System.out.print("Tool is now: " + currentTool);
			}
		});
		
		ButtonGroup toolSelectButtons = new ButtonGroup(dirtButton, emptyButton, rockButton, gemButton, p1TrollButton, p1GoblinButton, p2TrollButton, p2GoblinButton);
		dirtButton.setChecked(true);

		Label fileOptions = new Label("File options", skin);
		
		TextButton saveButton = new TextButton("Save", skin);
		saveButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				saveLevel();
			}
		});
		TextButton loadButton = new TextButton("Load", skin);

		FileHandle[] dirHandle = Gdx.files.local("data/").list();
		String[] loadOptions = new String[dirHandle.length];
		for(int fileIndex = 0 ; fileIndex < dirHandle.length ; fileIndex++){
			loadOptions[fileIndex] = dirHandle[fileIndex].name();
		}
		
		loadList = new SelectBox(loadOptions, skin);
		
		loadButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				loadLevel();
			}
		});
		
		TextButton clearButton = new TextButton("Clear", skin);
		clearButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				clearTiles();
			}
		});

		table.right().top();
		table.add(rowsLabel);
		table.add(numRowsSelect).width(200);
		table.row();
		
		table.add(colsLabel);
		table.add(numColsSelect).width(200);
		table.row();
		
		table.add(toolLabel);
		table.row();
		table.add(dirtButton);
		table.row();
		table.add(emptyButton);
		table.row();
		table.add(rockButton);
		table.row();
		table.add(gemButton);
		table.row();
		table.add(p1TrollButton);
		table.row();
		table.add(p1GoblinButton);
		table.row();
		table.add(p2GoblinButton);
		table.row();
		table.add(p2TrollButton);
		table.row();
		table.add(fileOptions);
		table.row();
		table.add(saveButton);
		table.row();
		table.add(loadButton);
		table.add(loadList).width(200);
		table.row();
		table.add(clearButton);
		table.debug();
		
		stage.addActor(table);
		
	}
	
	@Override
	public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
        camera.update();        
		
		mousePos.x = Gdx.input.getX();
		mousePos.y = Gdx.input.getY();
		mousePos.z = 0;
		camera.unproject(mousePos);
			
		
		game.batch.begin();		
		for (Tile[] tileRow : grid){
			for(Tile tile : tileRow){
				renderTile(tile);
			}
		}

		for(Tile[] tileRow: grid){
			for(Tile tile : tileRow){
				if(tile.contains(mousePos.x, mousePos.y)){
					if(currentTool == TileType.DIRT){
						game.batch.draw(GameAssets.dirtBlock, tile.x, tile.y);
					}
					else if(currentTool == TileType.EMPTY){
						game.batch.draw(GameAssets.emptyBlock, tile.x, tile.y);
					}
					else if(currentTool == TileType.ROCK){
						game.batch.draw(GameAssets.rock, tile.x, tile.y);
					}
					else if(currentTool == TileType.GEM){
						game.batch.draw(GameAssets.gem, tile.x, tile.y);
					}
					else if(currentTool == TileType.P1_TROLL){
						game.batch.draw(GameAssets.p1TrollTex, tile.x, tile.y, 0, 0, 25, 25);
					}
					else if(currentTool == TileType.P1_GOBLIN){
						game.batch.draw(GameAssets.p1GoblinTex, tile.x, tile.y, 0, 0,20,20);
					}
					else if(currentTool == TileType.P2_GOBLIN){
						game.batch.draw(GameAssets.p2GoblinTex, tile.x, tile.y, 0,0,20,20);
					}
					else if(currentTool == TileType.P2_TROLL){
						game.batch.draw(GameAssets.p2TrollTex, tile.x, tile.y,0,0,25,25);
					}
				}
			}
		}
		game.batch.end();
		
		sDrawer.begin(ShapeType.Line);
		sDrawer.setColor(Color.WHITE);
		for(Tile[] tileRow : grid){
			for(Tile tile : tileRow){
				sDrawer.rect(tile.x, tile.y, tile.width, tile.height);				
			}
		}
		sDrawer.end();
		
		if(Gdx.input.isTouched()){
			for(Tile[] tileRow : grid){
				for(Tile tile: tileRow){
					if(tile.contains(mousePos.x, mousePos.y)){
						tile.tileType = currentTool;
					}
				}
			}
		}
		
		checkForDimensionChanges();
		
		stage.act();		
		stage.draw();
		Table.drawDebug(stage);

	}
	
	public void checkForDimensionChanges(){
		int newRows = Integer.parseInt(numRowsSelect.getSelection());
		int newCols = Integer.parseInt(numColsSelect.getSelection());
		
		if(numRows != newRows || numCols != newCols){
			Tile[][] newGrid = new Tile[newRows][newCols];
			for(int row = 0; row < newRows; row++){
				for(int col=0; col < newCols; col++){
					if(row < numRows && col < numCols){
						newGrid[row][col] = grid[row][col];
					}
					else{
						newGrid[row][col] = new Tile(row * GameScreen.CELL_SIZE, col * GameScreen.CELL_SIZE, GameScreen.CELL_SIZE);
					}
				}
			}
			
			grid = newGrid;
			numRows = newRows;
			numCols = newCols;
		}
	}
	
	public void clearTiles(){
		for(Tile[] row : grid){
			for(Tile tile : row){
				tile.tileType = TileType.DIRT;
			}
		}
	}
	
	public void renderTile(Tile tile){
		if(tile.tileType == TileType.DIRT){
			game.batch.draw(GameAssets.dirtBlock, tile.x, tile.y);
		}
		else if(tile.tileType == TileType.EMPTY){
			game.batch.draw(GameAssets.emptyBlock, tile.x, tile.y);
		}
		else if(tile.tileType == TileType.ROCK){
			game.batch.draw(GameAssets.rock, tile.x, tile.y);
		}
		else if(tile.tileType == TileType.GEM){
			game.batch.draw(GameAssets.gem, tile.x, tile.y);
		}
		else if(tile.tileType == TileType.P1_TROLL){
			game.batch.draw(GameAssets.p1TrollTex, tile.x, tile.y, 0, 0, 25, 25);
		}
		else if(tile.tileType == TileType.P1_GOBLIN){
			game.batch.draw(GameAssets.p1GoblinTex, tile.x, tile.y, 0, 0, 20, 20);
		}
		else if(tile.tileType == TileType.P2_GOBLIN){
			game.batch.draw(GameAssets.p2GoblinTex, tile.x, tile.y, 0, 0, 20, 20);
		}
		else if(tile.tileType == TileType.P2_TROLL){
			game.batch.draw(GameAssets.p2TrollTex, tile.x, tile.y, 0, 0, 25, 25);
		}
	}
	
	public void saveLevel(){
		int numGems = 0;
		int numP1Goblins = 0;
		int numP1Trolls = 0;
		int numP2Goblins = 0;
		int numP2Trolls = 0;
		for (int row = 0; row < grid.length; row++){
			for(int col = 0; col < grid[row].length; col++){
				Tile currentTile = grid[row][col];
				if(currentTile.tileType == TileType.GEM){
					numGems++;
				}
				else if(currentTile.tileType == TileType.P1_GOBLIN){
					numP1Goblins++;
				}
				else if(currentTile.tileType == TileType.P1_TROLL){
					numP1Trolls++;
				}
				else if(currentTile.tileType == TileType.P2_GOBLIN){
					numP2Goblins++;
				}
				else if(currentTile.tileType == TileType.P2_TROLL){
					numP2Trolls++;
				}
			}
		}
		
		if(!(numGems > 0 &&
		   numP1Goblins == 1 &&
		   numP2Goblins == 1 &&
		   numP1Trolls == 1 &&
		   numP2Trolls == 1)){
			System.out.println("Level is not valid, does not contain all the necessary tiles. Aborting Save");
			return;
		}
		FileHandle[] dirHandle = Gdx.files.local("data/").list();
		int highestLevel = 0;
		for(FileHandle file : dirHandle){
			System.out.println(file.name());
			int levelNumber = Integer.parseInt(file.nameWithoutExtension().split("level")[1]);
			if(levelNumber > highestLevel){
				highestLevel = levelNumber;
			}
		}
		Json json = new Json();
		String levelJson = json.toJson(grid, Tile[][].class);
		System.out.println(levelJson);
		FileHandle handle = Gdx.files.local("data/level" + (highestLevel + 1) + ".json");
		handle.writeString(levelJson, false);
		
		dirHandle = Gdx.files.local("data/").list();
		String[] loadOptions = new String[dirHandle.length];
		for(int fileIndex = 0 ; fileIndex < dirHandle.length ; fileIndex++){
			loadOptions[fileIndex] = dirHandle[fileIndex].name();
		}
		
		loadList.setItems(loadOptions);
	}
	
	public void loadLevel(){
		Json jReader = new Json();
		Tile[][] newGrid = jReader.fromJson(Tile[][].class, Gdx.files.local("data/" + loadList.getSelection()));
		if(newGrid.length == grid.length && newGrid[0].length == grid[0].length){
			grid = newGrid;			
		}
		else{
			System.out.println("Loaded level dimensions different from currently set dimensions. Feature yet to be implemented");
		}
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		filledTex.dispose();
		listTex.dispose();
		GameAssets.dispose();

	}

}
