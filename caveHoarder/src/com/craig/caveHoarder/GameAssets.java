package com.craig.caveHoarder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GameAssets{
	static TextureRegion dirtBlock;
	static TextureRegion emptyBlock;
	static TextureRegion rock;
	private static Texture staticTiles;
	static Texture p1GoblinTex;
	static Texture p2GoblinTex;
	static Texture p1TrollTex;
	static Texture p2TrollTex;
	static Texture gem;
	static Texture destroyHighlightTex;
	static Texture destroySelectTex;
	static Texture createSelectTex;
	static Texture createHighlightTex;
	static Texture splashImage;
	static Texture instructions;
	
	public static void loadTextures(){
		staticTiles = new Texture(Gdx.files.internal("staticTiles.png"));
		dirtBlock = new TextureRegion(staticTiles, 0, 0, 32, 32);
		rock = new TextureRegion(staticTiles, 32, 0, 32, 32);
		emptyBlock = new TextureRegion(staticTiles, 64, 0, 32, 32);

    	
    	p1GoblinTex = new Texture(Gdx.files.internal("goblin.png"));
    	p2GoblinTex = new Texture(Gdx.files.internal("goblin2.png"));
    	
    	p1TrollTex = new Texture(Gdx.files.internal("trollAnim.png"));
    	p2TrollTex = new Texture(Gdx.files.internal("trollAnim2.png"));
    	gem = new Texture(Gdx.files.internal("gem.png"));
    	
    	splashImage = new Texture(Gdx.files.internal("splashImage.png"));
    	
    	Pixmap destroyHighlightPix = new Pixmap(GameScreen.CELL_SIZE, GameScreen.CELL_SIZE, Format.RGBA8888);
    	destroyHighlightPix.setColor(1,0,0,0.5f);
    	destroyHighlightPix.fillRectangle(0,0,destroyHighlightPix.getWidth(), destroyHighlightPix.getHeight());
    	destroyHighlightTex = new Texture(destroyHighlightPix);
    	destroyHighlightPix.dispose();
    	
    	Pixmap destroySelectPix = new Pixmap(GameScreen.CELL_SIZE, GameScreen.CELL_SIZE, Format.RGBA8888);
    	destroySelectPix.setColor(1,0,0,0.9f);
    	destroySelectPix.fillRectangle(0, 0, destroySelectPix.getWidth(), destroySelectPix.getHeight());
    	destroySelectTex = new Texture(destroySelectPix);
    	destroySelectPix.dispose();
    	
    	Pixmap createHighlightPix = new Pixmap(GameScreen.CELL_SIZE, GameScreen.CELL_SIZE, Format.RGBA8888);
    	createHighlightPix.setColor(0,0,1,0.5f);
    	createHighlightPix.fillRectangle(0,0,createHighlightPix.getWidth(), createHighlightPix.getHeight());
    	createHighlightTex = new Texture(createHighlightPix);
    	createHighlightPix.dispose();
    	
    	Pixmap createSelectPix = new Pixmap(GameScreen.CELL_SIZE, GameScreen.CELL_SIZE, Format.RGBA8888);
    	createSelectPix.setColor(0,0,1,0.9f);
    	createSelectPix.fillRectangle(0, 0, createSelectPix.getWidth(), createSelectPix.getHeight());
    	createSelectTex = new Texture(createSelectPix);
    	createSelectPix.dispose();
    	
    	instructions = new Texture(Gdx.files.internal("instructions.png"));
	}
	
	public static void dispose(){
		staticTiles.dispose();
		p1GoblinTex.dispose();
		p2GoblinTex.dispose();
		p1TrollTex.dispose();
		p2TrollTex.dispose();
		gem.dispose();
		destroyHighlightTex.dispose();
		createHighlightTex.dispose();
		destroySelectTex.dispose();
		createSelectTex.dispose();
		splashImage.dispose();
	}
}
