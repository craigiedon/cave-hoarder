package com.craig.caveHoarder;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

public class Grid{
	private Cell[][] cells;
	public int totalGems = 0;
	public int cellSize;
	private float terrainShiftProbability = 0.3f;
	ArrayList<Player> players;
	Sound terrainShiftSound;
	
	public Grid(Tile[][] tileMap){
		terrainShiftSound = Gdx.audio.newSound(Gdx.files.internal("sound/caveIn.wav"));
		cells = new Cell[tileMap.length][tileMap[0].length];
		cellSize = GameScreen.CELL_SIZE;
		players = new ArrayList<Player>();
		
		for(int i = 0; i < numRows(); i++){
			for(int j = 0; j < numCols(); j++){
				switch(tileMap[i][j].tileType){
				case DIRT:
					cells[i][j] = new Cell(i * (cellSize), j * (cellSize), cellSize);
					break;
				case ROCK:
					cells[i][j] = new IndestructibleRock(i * (cellSize), j * (cellSize), cellSize);
					break;
				case EMPTY:
					cells[i][j] = new Cell(i * (cellSize), j * (cellSize), cellSize);
					cells[i][j].destroyCell();
					break;
				case GEM:
					cells[i][j] = new Cell(i * (cellSize), j * (cellSize), cellSize);
					createGem(i, j);
					break;
				default:
					cells[i][j] = new Cell(i * (cellSize), j * (cellSize), cellSize);
					break;
				}
			}
		}
	}
	
	public void terrainShift(){
		float terrainShiftThreshold = 1 - MathUtils.random(1.0f);
		if(terrainShiftThreshold <= terrainShiftProbability){
			if(MathUtils.random(1) == 0){
				//RANDOM BLOCK DESTRUCTION
				boolean blockDestroyed = false;
				int attempts = 0;
				while(!blockDestroyed && attempts < 50){
					int gridRow = MathUtils.random(numRows() - 1);
					int gridCol = MathUtils.random(numCols() - 1);
					
					if(!isOccupied(gridRow, gridCol) && isDestructible(gridRow, gridCol) && !hasGem(gridRow, gridCol)){
						terrainShiftSound.play();
						cells[gridRow][gridCol].shiftDestroy();
						blockDestroyed = true;
					}
					
					attempts++;
				}
			}
			else{
				//RANDOM BLOCK CONSTRUCTION
				boolean blockCreated = false;
				int attempts = 0;
				while(!blockCreated && attempts < 50){
					int gridRow = MathUtils.random(numRows() - 1);
					int gridCol = MathUtils.random(numCols() - 1);
					
					if(!isOccupied(gridRow, gridCol) && isCreatable(gridRow, gridCol) && !hasGem(gridRow, gridCol)){
						terrainShiftSound.play();
						cells[gridRow][gridCol].shiftCreate();
						blockCreated = true;
					}
					
					attempts++;
				}
			}
		}
	}
	
	public boolean isTerrainShifting(){
		for(Cell[] cellRow : cells){
			for(Cell cell : cellRow){
				if(cell.isShifting){
					return true;
				}
			}
		}
		return false;
	}
	
	public void placeRandomFeatures(){
		int gemsToCreate = numRows() * numCols() / 20;
		int indestructibleRocks = numRows() * numCols() / 30;
		
		//Rocks creation
		int createdRocks = 0;
		while(createdRocks < indestructibleRocks){
			int rockRow = MathUtils.random(numRows() - 1);
			int rockCol = MathUtils.random(numCols() - 1);
			if(!isOccupied(rockRow, rockCol) && isDestructible(rockRow, rockCol)){
				cells[rockRow][rockCol] = new IndestructibleRock(rockRow * cellSize, rockCol * cellSize, cellSize);
				createdRocks++;
			}
		}
		
		//Gems creation
		int createdGems = 0;
		while(createdGems < gemsToCreate){
			int gemRow = MathUtils.random(numRows() - 1);
			int gemCol = MathUtils.random(numCols() - 1);
			if(!hasGem(gemRow, gemCol) && !isOccupied(gemRow, gemCol) && isDestructible(gemRow, gemCol)){
				createGem(gemRow, gemCol);
				createdGems++;
			}
		}
		
	}
	
	public void addPlayer(Player p){
		players.add(p);
	}
	
	public boolean isDestructible(int r, int c){
		return cells[r][c].isDestructible();
	}
	
	public boolean isOccupied(int r, int c){
		boolean occupied = false;
		for(Player p : players){
			if(p.troll.occupies(r, c) || p.goblin.occupies(r, c)){
				occupied = true;
			}
		}
		return occupied;
	}
	
	public boolean isCreatable(int r, int c){
		return !isOccupied(r,c) && isDestroyed(r,c);
	}
	
	public boolean isDestroyed(int r, int c){
		return cells[r][c].isDestroyed();
	}
	
	public void update(){
		for(Cell[] row : cells){
			for(Cell cell: row){
				cell.update(Gdx.graphics.getDeltaTime());
			}
		}
	}
	
	public void draw(SpriteBatch batch){
		batch.begin();
		for(Cell[] row : cells){
			for(Cell square: row){
				square.render(batch);
			}
		}
		
		for(Player player : players){
			player.render(batch);
		}
		batch.end();
	}
	
	public int numRows(){
		return cells.length;
	}
	
	public int numCols(){
		return cells[0].length;
	}
	
	public float getWidth(){
		return numRows() * cellSize;
	}
	
	public float getHeight(){
		return numCols() * cellSize;
	}
	
	public Cell getCell(int row, int column){
		return cells[row][column];
	}
	
	public void createGem(int row, int column){
		
		Cell cell = cells[row][column];
		if(!cell.hasGem()){
			cell.createGem();
			cell.destroyCell();
			totalGems += 1;
		}
		
	}
	
	public boolean hasGem(int row, int column){
		return cells[row][column].hasGem();
	}
	
	public void removeGem(int row, int column){
		Cell cell = cells[row][column];
		if(cell.hasGem()){
			cell.takeGem();
		}
	}
}
