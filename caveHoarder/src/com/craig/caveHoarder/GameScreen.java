package com.craig.caveHoarder;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;

public class GameScreen implements Screen {
        final CaveHoarder game;
        public static final int CELL_SIZE = 32;

        OrthographicCamera camera;
        Grid grid;
        Player playerOne;
        Player playerTwo;
        Player currentPlayer;
        InputMultiplexer playerInput;
        Array<InputProcessor> players;
        
        Stage stage;
        Table uiTable;
		Label playerInfo;
		Label turnInfo;
		Label diggingInfo;
		Label p1Label;
		Label p1Score;
		Label p2Label;
		Label p2Score;
		Label gemInfo;
		String selectedLevelFile;
		Skin skin;
		VerticalGroup vertGroup;
		Image instructions;
        
        public GameScreen(final CaveHoarder gam, String selectedLevelFile) {
        	this.game = gam;
        	this.selectedLevelFile = selectedLevelFile;
        	skin = game.skin;
        	GameAssets.loadTextures();
        	
        	camera = new OrthographicCamera();
        	camera.setToOrtho(false, 800, 480);
        	
        	Tile[][] levelData = loadLevel("data/" + selectedLevelFile);
        	
        	grid = new Grid(levelData);
        	
        	int p1GoblinRow = 0;
        	int p1GoblinCol = 0;
        	int p2GoblinRow = 0;
        	int p2GoblinCol = 0;
        	
        	int p1TrollRow = 0;
        	int p1TrollCol = 0;
        	int p2TrollRow = 0;
        	int p2TrollCol = 0;
        	
        	for(int row = 0; row < levelData.length; row++){
        		for(int col = 0; col < levelData[row].length; col++){
        			switch(levelData[row][col].tileType){
        			case P1_GOBLIN:
        				p1GoblinRow = row;
        				p1GoblinCol = col;
        				break;
        			case P1_TROLL:
        				p1TrollRow = row;
        				p1TrollCol = col;
        				break;
        			case P2_GOBLIN:
        				p2GoblinRow = row;
        				p2GoblinCol = col;
        				break;
        			case P2_TROLL:
        				p2TrollRow = row;
        				p2TrollCol = col;
        				break;
					default:
						break;
        			}
        		}
        	}
        	playerOne = new Player(1, grid, PlayerTurnState.MOVE_GOBLIN, p1GoblinRow, p1GoblinCol, p1TrollRow, p1TrollCol);
        	playerTwo = new Player(2, grid, PlayerTurnState.FINISHED, p2GoblinRow, p2GoblinCol, p2TrollRow, p2TrollCol);
        	grid.addPlayer(playerOne);
        	grid.addPlayer(playerTwo);
        	currentPlayer = playerOne;
        	
        	stage = new Stage();
        	Table parentTable = new Table();
        	parentTable.setFillParent(true);
        	parentTable.right();
        	uiTable = new Table();
        	uiTable.setBackground(skin.getDrawable("panelBG"));
        	
    		//Display status information
    		playerInfo = new Label("Player: " + currentPlayer.playerNumber, skin, "caveHoarderGame");
    		diggingInfo = new Label("Actions Left: " + currentPlayer.turnsLeft, skin, "caveHoarderGame");
    		p1Label = new Label("P1", skin, "caveHoarderGame");
    		p1Score = new Label(Integer.toString(playerOne.gemsCollected), skin, "caveHoarderGame");
    		p2Label = new Label("P2", skin,  "caveHoarderGame");
    		p2Score = new Label(Integer.toString(playerTwo.gemsCollected), skin, "caveHoarderGame");
    		
    		gemInfo = new Label("Gems to Win: " + (grid.totalGems / 2 + 1), skin, "caveHoarderGame");
    		
    		instructions = new Image(skin.getDrawable("instructionsGoblinDestroy"));
   		
    		
    		uiTable.add(playerInfo).colspan(16).expandX().left().padTop(20).padLeft(20);
    		uiTable.row();
    		uiTable.add(diggingInfo).colspan(16).left().padLeft(20);
    		uiTable.row();
    		uiTable.add(p1Label).left().padLeft(20);
    		uiTable.add(new Image(GameAssets.gem)).left().width(32);
    		uiTable.add(p1Score).left();
    		uiTable.row();
    		uiTable.add(p2Label).left().padLeft(20);
    		uiTable.add(new Image(GameAssets.gem)).left().width(32);
    		uiTable.add(p2Score).left();
    		uiTable.row();
    		uiTable.add(gemInfo).colspan(16).padLeft(20).left();
    		uiTable.row();
    		uiTable.add(instructions).pad(10,20,0,0);
    		uiTable.right().top();
    		
    		
    		parentTable.add(uiTable).height(480).width(300);
//    		parentTable.debug();
        	stage.addActor(parentTable);
        	
        	players = new Array<InputProcessor>();
        	players.add(playerOne);
        	players.add(playerTwo);
        	playerInput = new InputMultiplexer();
        	playerInput.setProcessors(players);
        	Gdx.input.setInputProcessor(playerInput);
        }
        
        public Tile[][] loadLevel(String filePath){
    		Json jReader = new Json();
    		Tile[][] loadedGrid = jReader.fromJson(Tile[][].class, Gdx.files.local(filePath));
    		return loadedGrid;
        }

        @Override
        public void render(float delta) {
        		//Render Code
                Gdx.gl.glClearColor(0.2f, 0.1f, 0.1f, 1);
                Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
                camera.update();
                game.batch.setProjectionMatrix(camera.combined);
        		stage.draw();

        		grid.draw(game.batch);
        		Table.drawDebug(stage);
        		
                //Update loop
                if(!checkWinCondition()){
                	playerOne.update();
                	playerTwo.update();
                	grid.update();
                	if(!grid.isTerrainShifting()){
                		playerInput.clear();
                		playerInput.addProcessor(playerOne);
                		playerInput.addProcessor(playerTwo);
                		updateGameInfo();
                		checkForTurnEnd();        			
                	}
                	else{
                		playerInput.clear();
                	}
                }
        }
        
        public void checkForTurnEnd(){
            if(currentPlayer.playerNumber == 1 && currentPlayer.state == PlayerTurnState.FINISHED){
            	grid.terrainShift();
            	currentPlayer.state = PlayerTurnState.AWAITING_TURN;
            	currentPlayer = playerTwo;
            	currentPlayer.startTurn();
            }
            else if(currentPlayer.playerNumber == 2 && currentPlayer.state == PlayerTurnState.FINISHED){
            	grid.terrainShift();
            	currentPlayer.state = PlayerTurnState.AWAITING_TURN;
            	currentPlayer = playerOne;
            	currentPlayer.startTurn();
            }
        }
        
        public void updateGameInfo(){
    		playerInfo.setText("Player: " + currentPlayer.playerNumber);
//    		turnInfo.setText("Turn Phase: " + currentPlayer.state.toString());
    		diggingInfo.setText("Actions Left: " + currentPlayer.turnsLeft);
    		p1Score.setText(Integer.toString(playerOne.gemsCollected));
    		p2Score.setText(Integer.toString(playerTwo.gemsCollected));
    		gemInfo.setText("Gems to Win: " + (grid.totalGems / 2 + 1));
    		
    		diggingInfo.setVisible(currentPlayer.turnsLeft != 0);
    		
    		if(currentPlayer.state == PlayerTurnState.MOVE_TROLL){
    			instructions.setDrawable(skin.getDrawable("instructionsTroll"));
    		}
    		else if(currentPlayer.goblin.currentTool == Tool.CREATE){
    			instructions.setDrawable(skin.getDrawable("instructionsGoblinCreate"));
    		}
    		else{
    			instructions.setDrawable(skin.getDrawable("instructionsGoblinDestroy"));
    		}
        }
        
        public boolean checkWinCondition(){
        	if(playerOne.gemsCollected > grid.totalGems / 2){
        		game.setScreen(new WinScreen(game, 1, selectedLevelFile));
        		return true;
        	}
        	else if(playerTwo.gemsCollected > grid.totalGems / 2){
        		game.setScreen(new WinScreen(game, 2, selectedLevelFile));
        		return true;
        	}
        	
        	return false;
        }
        
        @Override
        public void resize(int width, int height) {
        }

        @Override
        public void show() {
        }

        @Override
        public void hide() {
        }

        @Override
        public void pause() {
        }

        @Override
        public void resume() {
        }

        @Override
        public void dispose() {
        	GameAssets.dispose();
        }

}