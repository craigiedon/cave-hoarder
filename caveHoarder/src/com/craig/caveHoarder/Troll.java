package com.craig.caveHoarder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

public class Troll extends Rectangle {
	Grid gameGrid;
	float vx = 0;
	float vy = 0;
	float gravity = 10;
	float ax = 0;
	float ay = 0;
	float maxVx = 80;
	float maxVy = 200;
	boolean jumping = false;
	boolean onGround = false;
	TextureRegion trollLeft;
	TextureRegion trollRight;
	Directions facing;
	Animation walkLeft;
	Animation walkRight;
	TextureRegion jumpLeft;
	TextureRegion jumpRight;
	float animTime;
	Sound jumpSound;
	
	public Troll(int r, int c, Grid grid, int playerNum){
		jumpSound = Gdx.audio.newSound(Gdx.files.internal("sound/jump.wav"));
		gameGrid = grid;
		Cell startingCell = gameGrid.getCell(r, c);
		width = 25;
		height = 25;
		x = startingCell.x + (startingCell.width - width) / 2;
		y = startingCell.y + (startingCell.height - height) / 2;
		Array<TextureRegion> walkFramesLeft = new Array<TextureRegion>();
		Array<TextureRegion> walkFramesRight = new Array<TextureRegion>();
		
		if(playerNum == 1){
			for(int i = 0; i < 150; i+=width){
				walkFramesLeft.add(new TextureRegion(GameAssets.p1TrollTex, i, 25, (int)width, (int)height));
				walkFramesRight.add(new TextureRegion(GameAssets.p1TrollTex, i, 0, (int) width, (int) height));
			}
			jumpLeft = new TextureRegion(GameAssets.p1TrollTex, 0, 50, (int) width, (int) height);
			jumpRight = new TextureRegion(GameAssets.p1TrollTex, 25, 50, (int) width, (int) height);			
		}
		else{
			for(int i = 0; i < 150; i+=width){
				walkFramesLeft.add(new TextureRegion(GameAssets.p2TrollTex, i, 25, (int)width, (int)height));
				walkFramesRight.add(new TextureRegion(GameAssets.p2TrollTex, i, 0, (int) width, (int) height));
			}
			jumpLeft = new TextureRegion(GameAssets.p2TrollTex, 0, 50, (int) width, (int) height);
			jumpRight = new TextureRegion(GameAssets.p2TrollTex, 25, 50, (int) width, (int) height);
		}
		walkRight = new Animation(0.1f, walkFramesRight);
		walkLeft = new Animation(0.1f, walkFramesLeft);

		facing = Directions.EAST;
		
	}
	
	public void render(SpriteBatch batch){
			float frameTimeToUse = animTime;
        	if(facing == Directions.EAST){
        		if(onGround){
        			batch.draw(walkRight.getKeyFrame(frameTimeToUse), x, y);
        		}
        		else{
        			batch.draw(jumpRight, x, y);
        		}
        	}
        	else{
        		if(onGround){
            		batch.draw(walkLeft.getKeyFrame(frameTimeToUse), x, y);        			
        		}
        		else{
        			batch.draw(jumpLeft, x, y);
        		}
        	}
	}
	
	public void jump(){
		if(!jumping && onGround){
			jumpSound.play();
			jumping = true;
			vy = 220;
		}
	}
	
	public void update(){
		if(ax != 0){
			animTime += Gdx.graphics.getDeltaTime();
		}
		if(walkRight.isAnimationFinished(animTime)){
			animTime = 0;
		}
		vx += ax;
		vy += ay;
		vy -= gravity;
		
		if(vx > maxVx) vx = maxVx;
		if(vx < -maxVx) vx = -maxVx;
		if(vy > maxVy) vy = maxVy;
		if(vy < -maxVy) vy = -maxVy;
		
		if(ax == 0){
			vx *= 0.8;
		}		
		
		x += vx * Gdx.graphics.getDeltaTime();
		y += vy * Gdx.graphics.getDeltaTime();
		
		onGround = false;
		
		separateFromGrid();
		
		if(x < 0) x = 0;
		if(x + width > gameGrid.getWidth()) x = gameGrid.getWidth() - width;
		if(y < 0){
			y = 0;
			jumping = false;
			onGround = true;
			vy = 0;
		}
		if(y + height > gameGrid.getHeight()){
			y = gameGrid.getHeight() - height;
			vy = 0;
		}
		
	}
	
	public boolean occupies(int r, int c){
		return r == getRow() && c == getCol();
	}
	
	public int getRow(){
		return (int) Math.floor(getCentreX() / gameGrid.cellSize);
	}
	
	public int getCol(){
		return (int) Math.floor(getCentreY() / gameGrid.cellSize);
	}
	
	public float getCentreX(){
		return x + width / 2;
	}
	
	public float getCentreY(){
		return y + height / 2;
	}
	
	public void separateFromGrid(){
		for(int i = 0; i < gameGrid.numRows(); i++){
			for(int j = 0; j < gameGrid.numCols(); j++){
				Cell cell = gameGrid.getCell(i, j);
				if(!cell.isDestroyed()){
					if(cell.overlaps(this)){
					
							//Compute overlap
							float overlapX = 0;
							float overlapY = 0;
							if (x > cell.x){
								overlapX = cell.x + cell.width - x;
							}
							else{
								overlapX = x + width - cell.x;
							}	
							
							if(y > cell.y){
								overlapY = cell.y + cell.height - y;
							}
							else{
								overlapY = y + height - cell.y;
							}
							
							//Resolve collision
							if(overlapX < overlapY){
								if(x > cell.x){
									x += overlapX;
									vx = 0;
								}
								else{
									x -= overlapX;
									vx = 0;
								}
							}
							else{
								if(y < cell.y){
									y -= overlapY;
									vy = 0;
								}
								else{
									y += overlapY;
									vy = 0;
									jumping = false;
									onGround = true;
								}
							}
					}
				}
			}
		}
	}
}