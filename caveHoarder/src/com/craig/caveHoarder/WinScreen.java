package com.craig.caveHoarder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class WinScreen implements Screen {

	final CaveHoarder game;
	int winningPlayer;
	OrthographicCamera camera;
	Stage stage;
	Skin skin;
	String lastPlayedLevel;
	
	public WinScreen(CaveHoarder gam, int winner, String lpLev){
		game = gam;
		skin = game.skin;
		winningPlayer = winner;
		lastPlayedLevel = lpLev;
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 400);
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		Table table = new Table();
		table.setFillParent(true);
		
		String winText;
		if(winningPlayer == 1){
			winText = "Player one wins";
		}
		else if(winningPlayer == 2){
			winText = "Player two wins";
		}
		else{
			winText = "Draw";
		}

		Label winLabel = new Label(winText, skin, "caveHoarder");
		
		table.add(winLabel).padBottom(20);
		table.row();
		
		TextButton ReplayButton = new TextButton("Replay", skin, "caveHoarder");
		TextButton MainMenuButton = new TextButton("MainMenu", skin, "caveHoarder");
		
		ReplayButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				game.setScreen(new GameScreen(game, lastPlayedLevel));
			}
		});
		
		MainMenuButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				game.setScreen(new MenuScreen(game));
			}
		});
		table.add(ReplayButton).padBottom(20);
		table.row();
		table.add(MainMenuButton);
		stage.addActor(table);
		
	}
	@Override
	public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
        camera.update();

        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.setColor(1,1,1,0.5f);
        game.batch.draw(skin.getRegion("winScreen"),0, -20);
        game.batch.setColor(1,1,1,1);
        game.batch.end();
        stage.act();
        stage.draw();
        Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}

}
