package com.craig.caveHoarder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Json;

public class LevelSelect implements Screen {
	Skin skin;
	Stage stage;
	Texture listTex;
	CaveHoarder game;
	SelectBox levelSelectBox;
	OrthographicCamera camera;
	ButtonGroup availableLevels;
	public LevelSelect(CaveHoarder gam){
		stage = new Stage();
		game = gam;
		skin = game.skin;
		camera = new OrthographicCamera();
    	camera.setToOrtho(false, 800, 480);
    	GameAssets.loadTextures();
		
		Table levelTable = new Table();
//		levelTable.debug();
		levelTable.setFillParent(true);
		levelTable.top();
		Label levelSelectTitle = new Label("Choose a level", skin, "caveHoarder");
		levelTable.add(levelSelectTitle).colspan(4).pad(10, 0, 40, 0);
		levelTable.row();
		
		int rowCounter = 1;
		availableLevels = new ButtonGroup();
		for(FileHandle file : Gdx.files.local("data/").list()){
			LevelThumbNail levelThumb = new LevelThumbNail(loadLevel("data/" + file.name()), game, file.name());
			availableLevels.add(levelThumb);
			levelTable.add(levelThumb).pad(0,0,10,10);
			if(rowCounter % 4 == 0){
				levelTable.row();
			}
			rowCounter++;
		}
		
		TextButton startButton = new TextButton("Start", skin, "caveHoarder");
		startButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent clicked, float x, float y){
				game.setScreen(new GameScreen(game, ((LevelThumbNail)availableLevels.getChecked()).fileName));
			}
		});

		levelTable.row();
		levelTable.add(startButton).colspan(4).pad(20, 0, 0, 0);
//		Table table = new Table();
//		table.setFillParent(true);
//		table.bottom();
//		table.row();
//		table.add(startButton);
//		stage.addActor(table);
		stage.addActor(levelTable);
		Gdx.input.setInputProcessor(stage);
	}
	
    public Tile[][] loadLevel(String filePath){
		Json jReader = new Json();
		Tile[][] loadedGrid = jReader.fromJson(Tile[][].class, Gdx.files.local(filePath));
		return loadedGrid;
    }
    
	@Override
	public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
        camera.update();
        
        stage.draw();
        stage.act();
        Table.drawDebug(stage);

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
