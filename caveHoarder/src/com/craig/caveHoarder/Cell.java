package com.craig.caveHoarder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class Cell extends Rectangle {
	private boolean _isDestroyed = false;
	public boolean isShifting = false;
	float animationTime = 0;
	float animationDuration = 3;
	protected Rectangle gem = null;
	
	public void shiftDestroy(){
		_isDestroyed = true;
		isShifting = true;
	}
	
	public void shiftCreate(){
		_isDestroyed = false;
		isShifting = true;
	}
	
	public void destroyCell(){
		_isDestroyed = true;
	}
	
	public void createCell(){
		_isDestroyed = false;
	}
	
	public boolean isDestroyed(){
		return _isDestroyed;
	}
	
	public void createGem(){
		gem = new Rectangle(x, y, width, height);
	}
	
	public boolean hasGem(){
		return gem != null;
	}
	
	public void takeGem(){
		gem = null;
	}

    public boolean isDestructible(){
	    return !_isDestroyed;
	}
	
	public Cell(float x, float y, float sideLength){
		super(x,y, sideLength, sideLength);
	}
	
	public void update(float timeElapsed){
		if(isShifting){
			animationTime += timeElapsed;
			if(animationTime > animationDuration){
				isShifting = false;
				animationTime = 0;
			}
		}
	}
	
	public void render(SpriteBatch batch){

		
		float dirtOpacity = 1;
		float xJitter = 0;
		float yJitter = 0;
		if(isDestroyed()){
			dirtOpacity = 0;
		}
		else if(!isDestroyed()){
			dirtOpacity = 1;
		}
		
		if(isShifting){
			xJitter = MathUtils.random(-1.5f, 1.0f);
			yJitter = MathUtils.random(-1.5f, 1.0f);
			if(isDestroyed()){
				dirtOpacity = 1 - (animationTime / animationDuration);
			}
			else if(!isDestroyed()){
				dirtOpacity = (animationTime / animationDuration);
			}
		}
		

		batch.draw(GameAssets.emptyBlock, x, y);
		
		batch.setColor(1, 1, 1, dirtOpacity);
        batch.draw(GameAssets.dirtBlock, x + xJitter, y + yJitter);
        batch.setColor(1,1,1,1);
        
		if(hasGem()){
			batch.draw(GameAssets.gem, gem.x, gem.y);
		}
	}
}
