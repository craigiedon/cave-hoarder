package com.craig.caveHoarder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.Dictionary;
import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Goblin extends Rectangle{
	float vx = 0;
	float vy = 0;
	Grid gameGrid;
	Tool currentTool;
	Animation moveCycleCreate;
	Animation moveCycleDestroy;
	float animTime = 0;
	float currentRotation = 0;
	
	public Goblin(int r, int c, Grid grid, int playerNum){
		gameGrid = grid;
		currentTool = Tool.DESTROY;
		Cell startingCell = gameGrid.getCell(r, c);
		width = 20;
		height = 20;
		x = startingCell.x + (startingCell.width - width) / 2;
		y = startingCell.y + (startingCell.height - height) / 2;

		Array<TextureRegion> moveFramesDestroy = new Array<TextureRegion>();
		Array<TextureRegion> moveFramesCreate = new Array<TextureRegion>();

		if(playerNum == 1){
			for(int i = 0; i < 80; i+=20){
				moveFramesDestroy.add(new TextureRegion(GameAssets.p1GoblinTex, i, 0, (int) width, (int) height));
				moveFramesCreate.add(new TextureRegion(GameAssets.p1GoblinTex, i, 20, (int) width, (int) height));
			}
		}
		else if(playerNum == 2){
			for(int i = 0; i < 80; i+=20){
				moveFramesDestroy.add(new TextureRegion(GameAssets.p2GoblinTex, i, 0, (int) width, (int) height));
				moveFramesCreate.add(new TextureRegion(GameAssets.p2GoblinTex, i, 20, (int) width, (int) height));
			}
		}
		
		moveCycleCreate = new Animation(0.1f, moveFramesCreate);
		moveCycleDestroy = new Animation(0.1f, moveFramesDestroy);
	}
	
	public void update(){
		if(vx != 0 || vy != 0){
			animTime += Gdx.graphics.getDeltaTime();
		}
		if(moveCycleCreate.isAnimationFinished(animTime)){
			animTime = 0;
		}
		x += vx * Gdx.graphics.getDeltaTime();
		y += vy * Gdx.graphics.getDeltaTime();
		
		separateFromGrid();
		
		if(x < 0) x = 0;
		if(x + width > gameGrid.getWidth()) x = gameGrid.getWidth() - width;
		if(y < 0) y = 0;
		if(y + height > gameGrid.getHeight()) y = gameGrid.getHeight() - height;
	}
	
	public void render(SpriteBatch batch){

		if(vx < 0 && vy == 0){
			currentRotation = 90;
		}
		else if(vx > 0 && vy == 0){
			currentRotation = -90;
		}
		else if(vx == 0 && vy > 0){
			currentRotation = 0;
		}
		else if(vx == 0 && vy < 0){
			currentRotation = 180;
		}
		else if(vx < 0 && vy > 0){
			currentRotation = 45;
		}
		else if(vx > 0 && vy > 0){
			currentRotation = -45;
		}
		else if(vx < 0 && vy < 0){
			currentRotation = 135;
		}
		else if(vx > 0 && vy < 0){
			currentRotation = -135;
		}
		
		
		Sprite currentSprite;
		if(currentTool == Tool.DESTROY){
			currentSprite = new Sprite(moveCycleDestroy.getKeyFrame(animTime));
		}
		else{
			currentSprite = new Sprite(moveCycleCreate.getKeyFrame(animTime));
		}
		currentSprite.setRotation(currentRotation);
		currentSprite.setPosition(x, y);
		currentSprite.draw(batch);
	}
	
	public int getRow(){
		return (int) Math.floor(getCentreX() / gameGrid.cellSize);
	}
	
	public int getCol(){
		return (int) Math.floor(getCentreY() / gameGrid.cellSize);
	}
	
	public int[] blockAbove(){
		return new int[]{getRow(), getCol() + 1};
	}
	
	public int[] blockBelow(){
		return new int[]{getRow(), getCol() - 1};
	}
	
	public int[] blockLeft(){
		return new int[]{getRow() - 1, getCol()};
	}
	
	public int[] blockRight(){
		return new int[]{getRow() + 1, getCol()};
	}
	
	public float getCentreX(){
		return x + width / 2;
	}
	
	public float getCentreY(){
		return y + height / 2;
	}
	
	public boolean occupies(int r, int c){
		return r == getRow() && c == getCol();
	}
	
	public void separateFromGrid(){
		Intersector.MinimumTranslationVector mtv = new Intersector.MinimumTranslationVector();
		for(int i = 0; i < gameGrid.numRows(); i++){
			for(int j = 0; j < gameGrid.numCols(); j++){
				Cell cell = gameGrid.getCell(i, j);
				if(!cell.isDestroyed()){
					if(Intersector.overlapConvexPolygons(new float[]{x, y,
													   x + width, y,
													   x + width ,y + height,
													   x, y + height},
													  new float[]{cell.x, cell.y,
																  cell.x + cell.width, cell.y,
																  cell.x + cell.width, cell.y + cell.height,
																  cell.x, cell.y + cell.height},
													  mtv)){
						if (x > cell.x){
							x -= mtv.depth * mtv.normal.x;
						}
						else{
							x += mtv.depth * mtv.normal.x;
						}
						
						if(y > cell.y){
							y += mtv.depth * mtv.normal.y;
						}
						else{
							y -= mtv.depth * mtv.normal.y;
						}
					}
				}
			}
		}
	}


}
