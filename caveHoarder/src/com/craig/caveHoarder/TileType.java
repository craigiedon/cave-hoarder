package com.craig.caveHoarder;

public enum TileType {
EMPTY, DIRT, ROCK, GEM, P1_TROLL, P1_GOBLIN, P2_TROLL, P2_GOBLIN
}
