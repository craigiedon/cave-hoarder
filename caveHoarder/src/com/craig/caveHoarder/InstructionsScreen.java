package com.craig.caveHoarder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class InstructionsScreen implements Screen {
	final CaveHoarder game;
	OrthographicCamera camera;
	Stage stage;
	Skin skin;
	
	public InstructionsScreen(CaveHoarder gam){
		game = gam;
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 400);
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		Table table = new Table();
		table.setFillParent(true);
		
		LabelStyle instructionsStyle = new LabelStyle(game.font, Color.WHITE);
		Label aim = new Label("Aim of the game: Use your gremlin to dig a path through the rock underground so" +
				" that your troll can move around the cave and" +
				" collect more gems than your opponent", instructionsStyle);
		aim.setWrap(true);
		Label turnPhases = new Label("Turn Phases:\n" +
				"1. Move Goblin - Move your goblin around the cave using the directional buttons, then press space to continue\n" +
				"2. Destroy Ground - Use the directional buttons to choose one of the highlighted areas to destroy, then press space to confirm your choice." +
				" Press escape to go back to moving again.\n" +
				"3.Move Troll - Move your troll with the directional buttons and collect as many gems as you can! Press space to end your turn", instructionsStyle);
		turnPhases.setWrap(true);
		table.add(aim).padBottom(20).width(400);
		table.row();
		table.add(turnPhases).width(400);
		table.row();
		
		skin = new Skin();

		// Generate a 1x1 white texture and store it in the skin named "white".
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));

		// Store the default libgdx font under the name "default".
		skin.add("default", new BitmapFont());

		// Configure a TextButtonStyle and name it "default". Skin resources are stored by type, so this doesn't overwrite the font.
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.down = skin.newDrawable("white", Color.ORANGE);
		textButtonStyle.over = skin.newDrawable("white", Color.GREEN);
		textButtonStyle.font = skin.getFont("default");
		skin.add("default", textButtonStyle);
		
		TextButton backButton= new TextButton("Back", skin);
		backButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x , float y){
				game.setScreen(new MenuScreen(game));
			}
		});
		
		table.add(backButton);
		
		
		stage.addActor(table);
	}
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		camera.update();
		
		game.batch.setProjectionMatrix(camera.combined);
		
		camera.update();
		stage.draw();
		stage.act();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
