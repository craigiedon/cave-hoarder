package com.craig.caveHoarder;

public enum PlayerTurnState {
	MOVE_GOBLIN,USE_TOOL, MOVE_TROLL, FINISHED, AWAITING_TURN
}
